const cardContainer = document.querySelector(".container");

class Card {
  constructor(name, email, {id, title, body}) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
  }

  renderCard() {
    cardContainer.insertAdjacentHTML("beforeend", `
      <div class="post-card" data-card-id="${this.id}">
        <div class="post-card__header-wrap">
          <div class="post-card__author-wrap"> 
            <h3 class="post-card__author-name">${this.name}</h3>
            <span class="verified-account__icon">
              <svg viewBox="0 0 22 22"><g><path d="M20.396 11c-.018-.646-.215-1.275-.57-1.816-.354-.54-.852-.972-1.438-1.246.223-.607.27-1.264.14-1.897-.131-.634-.437-1.218-.882-1.687-.47-.445-1.053-.75-1.687-.882-.633-.13-1.29-.083-1.897.14-.273-.587-.704-1.086-1.245-1.44S11.647 1.62 11 1.604c-.646.017-1.273.213-1.813.568s-.969.854-1.24 1.44c-.608-.223-1.267-.272-1.902-.14-.635.13-1.22.436-1.69.882-.445.47-.749 1.055-.878 1.688-.13.633-.08 1.29.144 1.896-.587.274-1.087.705-1.443 1.245-.356.54-.555 1.17-.574 1.817.02.647.218 1.276.574 1.817.356.54.856.972 1.443 1.245-.224.606-.274 1.263-.144 1.896.13.634.433 1.218.877 1.688.47.443 1.054.747 1.687.878.633.132 1.29.084 1.897-.136.274.586.705 1.084 1.246 1.439.54.354 1.17.551 1.816.569.647-.016 1.276-.213 1.817-.567s.972-.854 1.245-1.44c.604.239 1.266.296 1.903.164.636-.132 1.22-.447 1.68-.907.46-.46.776-1.044.908-1.681s.075-1.299-.165-1.903c.586-.274 1.084-.705 1.439-1.246.354-.54.551-1.17.569-1.816zM9.662 14.85l-3.429-3.428 1.293-1.302 2.072 2.072 4.4-4.794 1.347 1.246z"></path></g></svg>
            </span>
            <span class="post-card__author-email">${this.email}</span>
          </div>
          <button class="close-button" data-card-id="${this.id}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,3H16.3H7.7H5A2,2 0 0,0 3,5V7.7V16.4V19A2,2 0 0,0 5,21H7.7H16.4H19A2,2 0 0,0 21,19V16.3V7.7V5A2,2 0 0,0 19,3M15.6,17L12,13.4L8.4,17L7,15.6L10.6,12L7,8.4L8.4,7L12,10.6L15.6,7L17,8.4L13.4,12L17,15.6L15.6,17Z" /></svg>
          </button>
        </div>
        <h3 class="post-card__title">${this.title}</h3>
        <p class="post-card__text">${this.body}</p>
      </div>
    `)
  }

}

async function getCardInfo() {

  try {
      const postsResponse = await fetch("https://ajax.test-danit.com/api/json/posts");
      const usersResponse = await fetch("https://ajax.test-danit.com/api/json/users");

      if (!postsResponse.ok || !usersResponse.ok) {
        throw new Error("Error getting data");
      }

      const posts = await postsResponse.json();
      const users = await usersResponse.json();

      posts
        .filter(post => {
          const user = users.find(elem => elem.id === post.userId);
          return user !== undefined;
        })
        .forEach(post => {
          const {name, email} = users.find(elem => elem.id === post.userId);
          new Card(name, email, post).renderCard();
        })
  }
  catch(error) {
    console.log(error);
  }
}

getCardInfo();

cardContainer.addEventListener("click", event => {
  
  if (event.target.closest(".close-button")) {
    const postId = event.target.closest(".post-card").dataset.cardId;

    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE"
    })
    .then(response => {
      
      if (response.ok) {
        const card = event.target.closest(".post-card");
        card.remove();
      } else {
        console.error("Error removing card")
      }
    })
    .catch(error => {
      console.error("Request execution error", error);
    })
  }
})